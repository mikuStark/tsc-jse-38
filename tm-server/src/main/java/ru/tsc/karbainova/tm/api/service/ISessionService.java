package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.model.Session;

import java.util.List;

public interface ISessionService extends IService<ru.tsc.karbainova.tm.model.Session> {
    boolean checkDataAccess(String login, String password);

    Session open(String login, String password);

    Session sign(Session session);

    List<Session> getListSessionByUserId(String userId);

    void close(Session session);

    void closeAll(Session session);

    void validate(Session session);

    void validate(Session session, Role role);

    //    void signOutByLogin(String login);
    void signOutByUserId(String userId);

}
