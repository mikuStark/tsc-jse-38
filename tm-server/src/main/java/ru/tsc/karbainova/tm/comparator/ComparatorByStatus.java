package ru.tsc.karbainova.tm.comparator;

import ru.tsc.karbainova.tm.api.entity.IHasStatus;

import java.util.Comparator;

public class ComparatorByStatus implements Comparator<IHasStatus> {
    private static final ComparatorByStatus instance = new ComparatorByStatus();

    private ComparatorByStatus() {
    }

    public static ComparatorByStatus getInstance() {
        return instance;
    }

    @Override
    public int compare(IHasStatus o1, IHasStatus o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getStatus().compareTo(o2.getStatus());
    }
}
