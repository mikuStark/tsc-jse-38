package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.ITaskService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.model.Session;
import ru.tsc.karbainova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint {
    private ITaskService taskService;
    private ServiceLocator serviceLocator;

    public TaskEndpoint() {
    }

    public TaskEndpoint(
            final ServiceLocator serviceLocator,
            final ITaskService taskService
    ) {
        this.serviceLocator = serviceLocator;
        this.taskService = taskService;
    }

    @WebMethod
    public void clearTask(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.clear();
    }

    @WebMethod
    public void createTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") @NonNull String name
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.create(session.getUserId(), name);
    }

    @WebMethod
    public void createTaskAllParam(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") @NonNull String name,
            @WebParam(name = "description") @NonNull String description
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.create(session.getUserId(), name, description);
    }

    @WebMethod
    public void addTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "task") Task task
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.add(session.getUserId(), task);
    }

    @WebMethod
    public void removeTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "task") Task task
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.remove(session.getUserId(), task);
    }

    @WebMethod
    public List<Task> findAllTask(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.findAll();
    }

    @WebMethod
    public List<Task> findAllTaskByUserId(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.findAll(session.getUserId());
    }

//    @WebMethod
//    public List<Task> findAllTaskByComparator(
//            @WebParam(name = "session") final Session session,
//            @WebParam(name = "comparator") Comparator<Task> comparator) {
//            serviceLocator.getSessionService().validate(session);
//        return taskService.findAll(userId, comparator);
//    }

    @WebMethod
    public void removeByIdTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "id") @NonNull String id
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.removeById(session.getUserId(), id);
    }

    @WebMethod
    public void removeByIndexTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") @NonNull Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Task findByIdTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "id") @NonNull String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.findById(session.getUserId(), id);
    }

    @WebMethod
    public Task findByIndexTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") @NonNull Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public void addAllTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "tasks") List<Task> tasks) {
        serviceLocator.getSessionService().validate(session);
        taskService.addAll(tasks);
    }
}
